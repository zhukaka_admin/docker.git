# crmeb-docker-zhu 


#### 介绍

#### 主要对于crmeb本地搭建环境开发
集成环境

php7.4 + nginx + mysql + swoole4.6.4

#### 软件架构
软件架构说明


#### 安装教程

1.  必须有docker环境
2.  在docker目录下执行 docker compose up 命令
#### 使用说明

1.  根目录下的web目录 是nginx的www目录
2.  直接在web目录下拉取自己的项目
3.  进入php docker内 
4.  cd 进入自己的项目目录 
5.  php think swoole 启动项目


#### 注意事项
1.  PHP 的dockerfile内   http://app.oxibar.cn/redis-4.1.0.tgz 和 http://app.oxibar.cn/swoole-src-4.6.6.tar.gz  为我自己服武器上传的压缩包，如需更改成自己的也可以，主要是因为官方的下载速度太慢会造成超时
2.  PHP 的dockerfile内 swoole_loader74.so 是我项目内的文件，如有不一样的php版本，更换即可
3.  nginx 内换自己项目的文件夹名称
4.  mysql 用户名为test 密码为 123456 项目内 ip 设置为 192.168.24.2 
5.  注意：mysql 如果需要电脑连接 ip为127.0.0.1 用户名：test 密码：123456 端口号：3302 
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
